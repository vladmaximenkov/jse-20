package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.IUserRepository;
import ru.vmaksimenkov.tm.api.service.IUserService;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.empty.EmptyEmailException;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyLoginException;
import ru.vmaksimenkov.tm.exception.empty.EmptyPasswordException;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.EmailExistsException;
import ru.vmaksimenkov.tm.exception.user.LoginExistsException;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.util.HashUtil;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        if (userRepository.existsByEmail(email)) throw new EmailExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }

    @Override
    public boolean existsByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean existsByLogin(final String login) {
        return userRepository.existsByLogin(login);
    }

    @Override
    public User findById(final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        userRepository.removeByLogin(userId, login);
    }

    @Override
    public void setPassword(final String userId, final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (!userRepository.existsById(userId, userId)) throw new UserNotFoundException();
        userRepository.setPasswordById(userId, userId, password);
    }

    @Override
    public User setRole(final String userId, final Role role) {
        final User user = findById(userId, userId);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    @Override
    public User updateUser(final String userId, final String firstName, final String lastName, final String middleName) {
        final User user = userRepository.findById(userId, userId);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
