package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.enumerated.Sort;
import ru.vmaksimenkov.tm.exception.entity.SortNotFoundException;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListSortCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list sorted";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT LIST SORTED]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        List<Project> projects;
        try {
            projects = serviceLocator.getProjectService().findAll(userId, Sort.getSort(TerminalUtil.nextLine()).getComparator());
        } catch (SortNotFoundException e) {
            System.err.println(e.getMessage() + " Default sort instead");
            projects = serviceLocator.getProjectService().findAll(userId);
        }
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s |%n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ".\t" + project);
            index++;
        }
    }

    @Override
    public String name() {
        return "project-list-sort";
    }

}
