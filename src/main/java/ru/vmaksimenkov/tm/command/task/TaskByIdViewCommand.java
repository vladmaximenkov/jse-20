package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdViewCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        showTask(serviceLocator.getTaskService().findById(userId, TerminalUtil.nextLine()));
    }

    @Override
    public String name() {
        return "task-view-by-id";
    }

}
