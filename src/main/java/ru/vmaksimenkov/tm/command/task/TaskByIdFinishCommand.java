package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();

        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        serviceLocator.getTaskService().finishTaskById(userId, TerminalUtil.nextLine());
    }

    @Override
    public String name() {
        return "task-finish-by-id";
    }

}
