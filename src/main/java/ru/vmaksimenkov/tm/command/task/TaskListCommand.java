package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.model.Task;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[TASK LIST]");
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |%n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        int index = 1;
        for (final Task task : serviceLocator.getTaskService().findAll(userId)) {
            System.out.println(index + ".\t" + task);
            index++;
        }
    }

    @Override
    public String name() {
        return "task-list";
    }
}
