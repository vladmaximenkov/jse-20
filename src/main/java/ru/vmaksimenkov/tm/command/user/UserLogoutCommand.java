package ru.vmaksimenkov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Log out of the system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String name() {
        return "user-logout";
    }
}
