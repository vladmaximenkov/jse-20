package ru.vmaksimenkov.tm.command.system;

import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments) {
            final String arg = argument.arg();
            if (isEmpty(arg)) continue;
            System.out.println(arg);
        }
    }

    @Override
    public String name() {
        return "arguments";
    }
}
