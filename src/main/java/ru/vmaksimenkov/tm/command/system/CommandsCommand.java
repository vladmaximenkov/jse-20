package ru.vmaksimenkov.tm.command.system;

import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class CommandsCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-c";
    }

    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (isEmpty(name)) continue;
            System.out.println(name);
        }
    }

    @Override
    public String name() {
        return "commands";
    }
}
