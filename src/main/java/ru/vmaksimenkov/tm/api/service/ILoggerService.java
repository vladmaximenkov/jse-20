package ru.vmaksimenkov.tm.api.service;

public interface ILoggerService {

    void command(String message);

    void debug(String message);

    void error(Exception e);

    void info(String message);

}
