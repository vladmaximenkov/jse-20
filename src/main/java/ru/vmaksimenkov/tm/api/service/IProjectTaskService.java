package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    void clearTasks(String userId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void removeProjectById(String userId, String projectId);

    void removeProjectByIndex(String userId, Integer projectIndex);

    void removeProjectByName(String userId, String projectName);

    Task unbindTaskFromProject(String userId, String taskId);

}
