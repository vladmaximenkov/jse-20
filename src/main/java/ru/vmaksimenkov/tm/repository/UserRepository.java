package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.IUserRepository;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(final String email) {
        for (final User user : list) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

    @Override
    public boolean existsByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public User findById(final String id) {
        for (final User user : list) {
            if (id.equals(user.getId())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public void removeByLogin(final String userId, final String login) {
        remove(findByLogin(login));
    }

    @Override
    public void setPasswordById(final String userId, final String id, final String password) {
        findById(userId, id).setPasswordHash(HashUtil.salt(password));
    }

}
